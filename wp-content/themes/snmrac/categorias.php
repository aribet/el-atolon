﻿<section class="categoria pasajeros">
	<h2>Vehículos de Pasajeros</h2>

	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">A</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/A.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Automóvil City Car</strong>
				Kia Morning, Hyundai I-10 o similar - c/aire - gasolina
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-maletaGrande2" title="2 maletas grandes"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 21.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 126.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>


	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">B</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/B.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Automóvil</strong>
				Ford Fiesta, Hyundai Hatchback, Toyota Yaris Sport o similar - c/aire - gasolina
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-maletaGrande2" title="2 maletas grandes"></span>
					<span class="icon-maletaChica1" title="1 maleta chica"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 25.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 150.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>


	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">C</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/C.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Automóvil Sedan</strong>
				Hyundai Sedan Gasolina o similar - Full - c/aire
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-maletaGrande2" title="2 maletas grandes"></span>
					<span class="icon-maletaChica1" title="1 maleta chica"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 31.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 186.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>


	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">D</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/D.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Automóvil </strong>
					Peugeot 301 Diesel, Toyota Corolla Mecánico o similar - Full - c/aire
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-maletaGrande2" title="2 maletas grandes"></span>
					<span class="icon-maletaChica1" title="1 maleta chica"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 38.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 228.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>


	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">E</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/E.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Automóvil Automático</strong>
				Hyundai Elantra, Toyota Corolla o similar - Full - c/aire - gasolina
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-maletaGrande2" title="2 maletas grandes"></span>
					<span class="icon-maletaChica1" title="1 maleta chica"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 47.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 282.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>


</section>
<section class="categoria trabajo">
	<h2>Vehículos de Trabajo</h2>

		<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">M1</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/m1.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Camioneta 4x4</strong>
				Nissan Navara Diesel, Toyota Hilux Diesel o similar - Full - c/aire
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-4wd" title="Traccion 4 ruedas"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 63.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 378.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>

	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">M2</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/m2.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Camioneta 4x2</strong>
				Toyota Hilux Diesel, Nissan Navara Diesel, o similar - Full - c/aire
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-4wd" title="Traccion 4 ruedas"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 45.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 270.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>

	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">M3</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/m3.png" alt="">
			</div>
			<div class="descripcion">
				<strong>Van (7, 9 y 12 pasajeros)</strong>
				Peugeot Expert o similar - Full - c/aire - gasolina - diesel
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros9" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-maletaGrande3" title="3 maletas grandes"></span>
					<span class="icon-maletaChica3" title="3 maletas chica"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 65.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 390.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>

</section>