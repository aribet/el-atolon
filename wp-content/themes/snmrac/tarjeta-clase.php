<article>
	<div class="clase">
		<div class="etiqueta">Clase</div>
		<div class="tipo"><?php the_field('clase'); ?></div>
	</div>
	<div class="datos">
		<div class="foto">
			<img src="<?php the_field('imagen_representativa'); ?>" alt="<?php the_field('imagen_representativa'); ?>">
		</div>
		<div class="descripcion">
			<h3><?php the_title();?></h3>
			<?php the_content();?>
		</div>
		<div class="propiedades">
			<div class="iconos">
				<span 	class="icon-pasajeros-<?php the_field('pasajeros'); ?>"
						title="<?php the_field('pasajeros'); ?> pasajeros"></span>
				<span 	class="icon-puerta-<?php the_field('puertas'); ?>"
						title="<?php the_field('puertas'); ?> puertas"></span>
				<span 	class="icon-maletaGrande-<?php the_field('maleta_grande'); ?>"
      					title="<?php the_field('maleta_grande'); ?> maletas grandes"></span>
				<span 	class="icon-maletaChica-<?php the_field('maleta_chica'); ?>"
      					title="<?php the_field('maleta_chica'); ?> maletas chicas"></span>
				<span 	class="icon-aire-<?php the_field('aire');?>"
						title="<?php the_field('aire');?> Aire Acondicionado"></span>
				<span 	class="icon-airbag-<?php the_field('airbag');?>"
						title="con Airbag"></span>
				<span 	class="icon-<?php the_field('traccion');?>-wd"
						title="Tracción a <?php the_field('traccion');?> ruedas"></span>
			</div>
			<div class="valores">
				<div class="dia">
					<div class="texto">Valor Día:</div>
					<div class="precio">$ <?php the_field('valor_dia'); ?></div>
				</div>
				<div class="semanal">
					<div class="texto">Valor Semanal:</div>
					<div class="precio">$ <?php the_field('valor_semanal'); ?></div>
				</div>
			</div>
		</div>
	</div>
</article>