<!DOCTYPE html>

<html lang="en">

<head>

	<title><?php bloginfo('name') ?></title>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">

 	<link href="<?php bloginfo('stylesheet_url') ?>" rel="stylesheet" type="text/css" />
</head>
<body>
	<?php get_header(); ?>
	<div class="flota">
		<!-- seccion de vehiculos de pasajeros -->
		<section class="categoria pasajeros">
			<h2>Vehículos de Pasajeros</h2>

			<?php query_posts('cat=2&meta_key=clase&orderby=meta_value&order=asc');?>
			<?php while ( have_posts() ) : the_post();?>

			<?php get_template_part( 'tarjeta-clase', get_post_format() );?>

			<?php endwhile; // end of the loop. ?>

		</section>
		<!-- seccion de vehiculos de trabajo -->
		<section class="categoria trabajo">
			<h2>Vehículos de Trabajo</h2>

			<?php wp_reset_query()?> <!-- reseteo del query para evitar problemas -->
			<?php query_posts('cat=3&meta_key=clase&orderby=meta_value&order=asc');?>
			<?php while ( have_posts() ) : the_post();?>

			<?php get_template_part( 'tarjeta-clase', get_post_format() );?>

			<?php endwhile; // end of the loop. ?>
		</section>

	</div>

	<!-- seccion de CONTACTO -->
	<div class="contacto">
		<?php wp_reset_query();?>
		<?php query_posts('cat=4');?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
		<!-- post navigation -->
		<?php else: ?>
		<!-- no posts found -->
		<?php endif; ?>
	</div>
	<?php get_footer(); ?>
</body>

</html>