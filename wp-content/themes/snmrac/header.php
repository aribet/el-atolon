﻿<header>
	<div class="logo">
	     <img src="<?php bloginfo('template_directory') ?>/images/logo.png" alt="San Martin - Rent a Car">
    </div>
	<div class="reserva">
		<div class="texto">Reservas</div>
		<div class="central marco">800 100 800</div>
		<div class="fono marco">(56) (41) 279 93 99</div>
		<div class="mail marco"><a href="mailto:contacto@sanmartinrentacar.cl">contacto@sanmartinrentacar.cl</a></div>
	</div>
</header>
