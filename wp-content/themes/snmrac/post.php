
	<article>
		<div class="clase">
			<div class="etiqueta">Clase</div>
			<div class="tipo">A</div>
		</div>
		<div class="datos">
			<div class="foto">
				<img src="<?php bloginfo('template_directory');?>/images/automovil/A.png" alt="">
			</div>
			<div class="descripcion">
				Automóvil City Car
				Kia Morning, Hyundai I-10 o similar - c/aire - gasolina
			</div>
			<div class="propiedades">
				<div class="iconos">
					<span class="icon-pasajeros4" title="4 pasajeros"></span>
					<span class="icon-puerta" title="4 puertas"></span>
					<span class="icon-maletaGrande2" title="2 maletas grandes"></span>
					<span class="icon-maletaChica1" title="1 maleta chica"></span>
					<span class="icon-airbag" title="Airbag"></span>
					<span class="icon-aire" title="Aire Acondicionado"></span>
				</div>
				<div class="valores">
					<div class="dia">
						<div class="texto">Valor Día:</div>
						<div class="precio">$ 21.000</div>
					</div>
					<div class="semanal">
						<div class="texto">Valor Semanal:</div>
						<div class="precio">$ 126.000</div>
					</div>
				</div>
			</div>
		</div>
	</article>