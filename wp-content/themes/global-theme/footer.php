
        <footer>
            <div class="grupo">
                <div class="datos">
                    <p><strong>Globalgest</strong></p>
                    <p class="direccion"><i class="icon-map-marker"></i>Bombero Ossa 1010, Oficina 1026, Santiago Centro, Chile</p>
                    <p class="fono"><i class="icon-phone"></i>+56-02-26727242</p>
                    <p class="mail"> <a href="mailto:contacto@global-ges.cl"><i class="icon-envelope"></i>contacto@global-ges.cl</a></p>
                    <p class="web"> <a href="http://www.global-ges.cl"><i class="icon-globe"></i>www.global-ges.cl</a></p>
                </div>
                <div class="logos">
                    <figure class="iso">
                        <img width="115px" height="80px" src="<?php bloginfo("template_directory") ?>/img/ISO_9001-C.png" alt="">
                    </figure>
                    <figure class="nch">
                        <img  width="118px" height="80px" src="<?php bloginfo("template_directory") ?>/img/NCh2728-C.png" alt="">
                    </figure>
                </div>
            </div>
        </footer>
         </div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
        
                    <?php wp_footer() ?>
    </body>

</html>
