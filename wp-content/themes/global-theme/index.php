<?php get_header();?>

<div class="contenido">
	<?php get_sidebar(izq) ?>

	<section class="section">
		<?php wp_reset_query(); ?>
		<?php if (have_posts()) :   while (have_posts()) : the_post();?>


		<article>
<?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID), "mid"); ?>
<img class="figura" src="<?php echo $thumbnail_src[0]; ?>" alt="">
			<h1><?php the_title(); ?></h1>
			<div class="contenido_post"><?php the_content(); ?></div>
		</article>

		<?php   endwhile; 	endif; ?>

		</section>

	<?php get_sidebar(der);?>

</div>
<?php get_footer();?>
