<!DOCTYPE html>

<!--Etiquetas HTML5 para IE8 hacia abajo-->
<!--[if lt IE 9]>
<script type="text/javascript">
   document.createElement("nav");
   document.createElement("header");
   document.createElement("footer");
   document.createElement("section");
   document.createElement("article");
   document.createElement("aside");
   document.createElement("hgroup");
   document.createElement("figure");
</script>
<![endif]-->

<html class="no-js" style="margin:0 !important;">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>GLOBALGEST</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1"/> <!-- para responsive -->

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php bloginfo("template_directory") ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php bloginfo("template_directory") ?>/css/font-awesome.css">
        <link rel="stylesheet" href="<?php bloginfo("stylesheet_url") ?>">

<?php wp_head() ?>

    <!-- fuentes de google -->
    <link href='http://fonts.googleapis.com/css?family=Forum' rel='stylesheet' type='text/css'>
      
    </head>

    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <div class="contenedor">
        <header>
            <div class="grupo">

                <figure class="logotipo">
                    <img src="<?php bloginfo("template_directory") ?>/img/marca.png" alt="">
                </figure>
                
                <div class="contenedor_frases">
                    <iframe src="<?php bloginfo("template_directory") ?>/frases/index_animate.html" frameborder="0"></iframe>
                </div>
                <nav>
                   <?php wp_nav_menu(array(
                       'theme_location'  => 'main-nav-menu',
                       'menu'            => '', 
                       'container'       => 'span', 
                       'container_class' => 'menu-{menu slug}-container', 
                       'container_id'    => '',
                       'menu_class'      => 'menu', 
                       'menu_id'         => '',
                       'echo'            => true,
                       'fallback_cb'     => 'wp_page_menu',
                       'before'          => '',
                       'after'           => '',
                       'link_before'     => '',
                       'link_after'      => '',
                       'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                       'depth'           => 0,
                       'walker'          => '')); ?>
                </nav>
            </div>
        </header>