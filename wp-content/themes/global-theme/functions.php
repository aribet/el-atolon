<?php 

add_action('init', 'register_my_menus');

function register_my_menus(){
	register_nav_menus(
		array(

			'main-nav-menu' => __('Menu Superior'),
			'sidebar-menu' => __('Menu Lateral'),
		)
	);	
}


// imagenes destacadas
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
}



 ?>